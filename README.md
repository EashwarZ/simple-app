# Simple-App - integration of gitlab with jenkins merge_when_pipeline_succeeds
Step-1: Setup GitLab Repository:
   -> a simple python file to print message is set up in the gitlab repository
   -> the gitlab readme file is updated

Step-2: Jenkins pipeline and integration

    -> a jenkins pipeline project is created 
    -> a python running script is created in the pipeline script
    -> build triggers are turned on to initiate a build whenever a change is pushed in the gitlab repository

Step-3: move jenkins from localhost(ngrok):
    -> by using ngrok.. (./ngrok http 8080)
    -> copy the http or https url for the jenkins to work in the web browser    


Step-4: Gitlab integration with jenkins and project set up:
    
    -> a python project is pushed to the gitlab repository
    -> in the settings page on the left side of the panel ---> click on the Integrations option
    -> provide the jenkins url received from the ngrok 
    -> provide jenkins user id and password

Step-5: trigger from gitlab:
    -> whenever a change is commited in the gitlab the pipeline starts to build in the jenkins server
    -> the screenshots are attached in the file

Step-6: creating gitlab pipeline:
    -> a gitlab pipeline is created using the build pipeline option and
    -> a .gitlab-ci.yml file is created

Step-7: Testing:
    -> whenever a change is pushed in gitlab
    -> the jenkins automatically triggers the build
    -. mentioned as push from gitlab by eashwar

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
